import random
import numpy as np
import matplotlib.pyplot as plt

class Network(object):

    def __init__(self, sizes):
        
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = []
        self.weights = []

    def parameters_deep(self, layer_dims):
        
        parameters = {}
        #layer_dims = self.num_layers
        L = len(layer_dims)            # number of layers in the network

        for l in range(1, L):
        ### START CODE HERE ### (≈ 2 lines of code)
            parameters['W' + str(l)]= np.random.randn(layer_dims[l],layer_dims[l-1])*0.01
            parameters['b' + str(l)]= np.zeros((layer_dims[l],1))
        ### END CODE HERE ###
        
        assert(parameters['W' + str(l)].shape == (layer_dims[l], layer_dims[l-1]))
        assert(parameters['b' + str(l)].shape == (layer_dims[l], 1))

        return parameters

    
#tính forward
    def L_model_forward(self, X, parameters):
    
        caches = []
        A = X
        L = len(parameters) // 2                  # number of layers in the neural network
        #L = self.num_layers
        # Implement [LINEAR -> RELU]*(L-1). Add "cache" to the "caches" list.
        #for l in range(0, L-2):
        for l in range(1, L):
            A_prev = A 
            ### START CODE HERE ### (≈ 2 lines of code)
            A, cache = self.linear_activation_forward(A_prev,parameters['W'+str(l)],parameters['b'+str(l)],activation='relu')
            #A, cache = self.linear_activation_forward(A_prev, self.weights[l], self.biases[l], activation = 'relu')
            caches.append(cache)
            ### END CODE HERE ###
    
        # Implement LINEAR -> SIGMOID. Add "cache" to the "caches" list.
        ### START CODE HERE ### (≈ 2 lines of code)
        #AL, cache = self.linear_activation_forward(A, self.weights[L-2], self.biases[L-2], activation = 'sigmoid')
        AL, cache = self.linear_activation_forward(A,parameters['W'+str(L)],parameters['b'+str(L)],activation='sigmoid')
        caches.append(cache)
        ### END CODE HERE ###
    
        #assert(AL.shape == (1,X.shape[1]))
            
        return AL, caches

    def linear_activation_forward(self, A_prev, W, b, activation):
        
        if activation == "sigmoid":
            # Inputs: "A_prev, W, b". Outputs: "A, activation_cache".
            ### START CODE HERE ### (≈ 2 lines of code)
            Z = np.dot(W,A_prev)+b
            linear_cache = (A_prev, W, b)
            #Z, linear_cache = linear_forward(A_prev,W,b)
            A, activation_cache = sigmoid(Z)
            ### END CODE HERE ###
        elif activation == "relu":
            # Inputs: "A_prev, W, b". Outputs: "A, activation_cache".
            ### START CODE HERE ### (≈ 2 lines of code)
            Z = np.dot(W,A_prev)+b
            linear_cache = (A_prev, W, b)
            #Z, linear_cache = linear_forward(A_prev,W,b)
            A, activation_cache = relu(Z)
            ### END CODE HERE ###
    
        assert (A.shape == (W.shape[0], A_prev.shape[1]))
        cache = (linear_cache, activation_cache)

        return A, cache

# tính cost

    def compute_cost(self, AL, Y):
       
        m = Y.shape[1]

        # Compute loss from aL and y.
        ### START CODE HERE ### (≈ 1 lines of code)
        cost = (-1/m)*(np.sum(np.multiply(Y,np.log(AL))+np.multiply(1-Y,np.log(1-AL)),axis=1,keepdims=True))
        ### END CODE HERE ###
    
        cost = np.squeeze(cost)      # To make sure your cost's shape is what we expect (e.g. this turns [[17]] into 17).
        #assert(isinstance(cost, float))
        #assert(cost.shape == ())
    
        return cost

# Tinh Backward

    def linear_backward(self, dZ, cache):
    
        A_prev, W, b = cache
        m = A_prev.shape[1]

        ### START CODE HERE ### (≈ 3 lines of code)
        dW = (1/m)*np.dot(dZ,A_prev.T)
        db = (1/m)*np.sum(dZ,axis=1,keepdims=True)
        dA_prev = np.dot(W.T,dZ)
        ### END CODE HERE ###
    
        assert (dA_prev.shape == A_prev.shape)
        assert (dW.shape == W.shape)
        assert (db.shape == b.shape)
    
        return dA_prev, dW, db


    def linear_activation_backward(self, dA, cache, activation):
    
        linear_cache, activation_cache = cache
    
        if activation == "relu":
            ### START CODE HERE ### (≈ 2 lines of code)
            dZ = relu_backward(dA, activation_cache)
            dA_prev, dW, db = self.linear_backward(dZ,linear_cache)
            ### END CODE HERE ###
        
        elif activation == "sigmoid":
            ### START CODE HERE ### (≈ 2 lines of code)
            dZ = sigmoid_backward(dA, activation_cache)
            dA_prev, dW, db = self.linear_backward(dZ,linear_cache)
            ### END CODE HERE ###
    
        return dA_prev, dW, db

    def L_model_backward(self, AL, Y, caches):
    
        grads = {}
        L = len(caches) # the number of layers
        m = AL.shape[1]
        Y = Y.reshape(AL.shape) # after this line, Y is the same shape as AL
    
        # Initializing the backpropagation
        ### START CODE HERE ### (1 line of code)
        dAL = - (np.divide(Y, AL) - np.divide(1 - Y, 1 - AL))
        ### END CODE HERE ###
    
        # Lth layer (SIGMOID -> LINEAR) gradients. Inputs: "dAL, current_cache". Outputs: "grads["dAL-1"], grads["dWL"], grads["dbL"]
        ### START CODE HERE ### (approx. 2 lines)
        current_cache = caches[L-1]
        grads["dA" + str(L-1)], grads["dW" + str(L)], grads["db" + str(L)] = self.linear_activation_backward(dAL,current_cache,activation='sigmoid')
        ### END CODE HERE ###
    
        # Loop from l=L-2 to l=0
        for l in reversed(range(L-1)):
            # lth layer: (RELU -> LINEAR) gradients.
            # Inputs: "grads["dA" + str(l + 1)], current_cache". Outputs: "grads["dA" + str(l)] , grads["dW" + str(l + 1)] , grads["db" + str(l + 1)] 
            ### START CODE HERE ### (approx. 5 lines)
            current_cache = caches[l]
            dA_prev_temp, dW_temp, db_temp = self.linear_activation_backward(grads["dA" + str(l+1)],current_cache,activation='relu')
            grads["dA" + str(l)] = dA_prev_temp
            grads["dW" + str(l + 1)] = dW_temp
            grads["db" + str(l + 1)] = db_temp
            ### END CODE HERE ###

        return grads
    def update_parameters(self, parameters, grads, learning_rate):
        
        L = len(parameters) // 2 # number of layers in the neural network

        # Update rule for each parameter. Use a for loop.
        ### START CODE HERE ### (≈ 3 lines of code)
        for l in range(L):
            parameters["W" + str(l+1)] = parameters["W" + str(l+1)] - learning_rate*grads['dW'+str(l+1)]
            parameters["b" + str(l+1)] = parameters["b" + str(l+1)] - learning_rate*grads['db'+str(l+1)]
            ### END CODE HERE ###
        return parameters

    def predict(self,X, y, parameters):
        
        m = X.shape[1]
        n = len(parameters) // 2 # number of layers in the neural network
        p = np.zeros((1,m))
    
        # Forward propagation
        probas, caches = self.L_model_forward(X, parameters)

    
        # convert probas to 0/1 predictions
        for i in range(0, probas.shape[1]):
            if probas[0,i] > 0.5:
                p[0,i] = 1
            else:
                p[0,i] = 0
    
        #print results
        #print ("predictions: " + str(p))
        #print ("true labels: " + str(y))
        print("Accuracy: "  + str(np.sum((p == y)/m)))
        
        return p

    def Train_L_layer_model(self, X, Y, learning_rate = 0.1, num_iterations = 2000, print_cost=False):#lr was 0.009
    
        np.random.seed(1)
        costs = []                         # keep track of cost
        lr = learning_rate
        # Parameters initialization. (≈ 1 line of code)
        ### START CODE HERE ###
        parameters = self.parameters_deep(self.sizes)
        ### END CODE HERE ###
    
        # Loop (gradient descent)
        for i in range(0, num_iterations):

            # Forward propagation: [LINEAR -> RELU]*(L-1) -> LINEAR -> SIGMOID.
            ### START CODE HERE ### (≈ 1 line of code)
            AL, caches = self.L_model_forward(X,parameters)
            ### END CODE HERE ###
        
            # Compute cost.
            ### START CODE HERE ### (≈ 1 line of code)
            cost = self.compute_cost(AL,Y)
            ### END CODE HERE ###
    
            # Backward propagation.
            ### START CODE HERE ### (≈ 1 line of code)
            grads = self.L_model_backward(AL,Y,caches)
            ### END CODE HERE ###
 
            # Update parameters.
            ### START CODE HERE ### (≈ 1 line of code)
            parameters = self.update_parameters(parameters,grads,learning_rate)
            ### END CODE HERE ###
                
            # Print the cost every 100 training example
            
            if print_cost and i % 100 == 0:
                print("Cost after iteration %i: %f" %(i, cost.sum()))
            if print_cost and i % 100 == 0:
                costs.append(cost)
            
        # plot the cost
        plt.plot(np.squeeze(costs))
        plt.ylabel('cost')
        plt.xlabel('iterations (per tens)')
        plt.title("Learning rate =" + str(learning_rate))
        plt.show()
    
        return parameters

def sigmoid(Z):

    A = 1/(1+np.exp(-Z))
    cache = Z
    
    return A, cache

def relu(Z):
        
    A = np.maximum(0,Z)
    
    assert(A.shape == Z.shape)
    
    cache = Z 
    return A, cache


def relu_backward(dA, cache):
   
    Z1 = cache
    dZ = np.array(dA, copy=True) # just converting dz to a correct object.
    
    # When z <= 0, you should set dz to 0 as well. 
    dZ[Z1 <= 0] = 0
    
    assert (dZ.shape == Z1.shape)
    
    return dZ

def sigmoid_backward(dA, cache):
       
    Z = cache
    
    s = 1/(1+np.exp(-Z))
    dZ = dA * s * (1-s)
    
    assert (dZ.shape == Z.shape)
    
    return dZ


