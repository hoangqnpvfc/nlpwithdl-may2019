#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 17:41:43 2018

@author: dtvo
"""
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from torch.autograd import Variable

class Embs(nn.Module):
    """
    This module take embedding inputs (characters or words) feeding to an RNN layer to extract:
        - all hidden features
        - last hidden features
        - all attentional hidden features
        - last attentional hidden features
    """
    def __init__(self, HPs):
        super(Embs, self).__init__()
        [nnmode, size, dim, pre_embs, hidden_dim, dropout, layers, bidirect, zero_padding, attention, use_cuda] = HPs
        #self.device = torch.device("cuda:0" if self.args.use_cuda else "cpu")
        self.zero_padding = zero_padding
        rnn_dim = hidden_dim // 2 if bidirect else hidden_dim
            
        self.embeddings = nn.Embedding(size, dim, padding_idx=0)
        if pre_embs is not None:
            self.embeddings.weight.data.copy_(torch.from_numpy(pre_embs))
        else:
            self.embeddings.weight.data.copy_(torch.from_numpy(self.random_embedding(size, dim)))

        self.drop = nn.Dropout(dropout)

        if nnmode == "rnn":
            self.hidden_layer = nn.RNN(dim, rnn_dim, num_layers=layers, batch_first=True, bidirectional=bidirect)
        elif nnmode == "gru":
            self.hidden_layer = nn.GRU(dim, rnn_dim, num_layers=layers, batch_first=True, bidirectional=bidirect)
        else: 
            self.hidden_layer = nn.LSTM(dim, rnn_dim, num_layers=layers, batch_first=True, bidirectional=bidirect)
        
        self.attention = attention
        self.tanh = nn.Tanh()
        if attention:
            self.att_hidden = nn.Linear(hidden_dim, hidden_dim)
            self.att_alpha = nn.Linear(hidden_dim,1, bias=False)
            self.att_norm = nn.Softmax(-1)
        if use_cuda:
            self.embeddings = self.embeddings.cuda()
            self.drop = self.drop.cuda()
            if attention:  
                self.att_hidden = self.att_hidden.cuda(torch.device('cuda:0'))
                self.att_norm = self.att_norm.cuda()
            #self.softmax = self.softmax.cuda()
                self.att_alpha = self.att_alpha.cuda()
            self.hidden_layer = self.hidden_layer.cuda(torch.device('cuda:2'))
            
    def forward(self, inputs, input_lengths):
        return self.get_last_hiddens(inputs, input_lengths)

    def get_last_hiddens(self, inputs, input_lengths):
        """
            input:  
                inputs: tensor(batch_size, seq_length)
                input_lengths: tensor(batch_size,  1)
            output: 
                tensor(batch_size, hidden_dim)
        """
        if self.zero_padding:
            # set zero vector for padding, unk, eot, sot
            self.set_zeros([0, 1, 2, 3])
        batch_size = inputs.size(0)
        seq_length = inputs.size(1)
        # embs = tensor(batch_size, seq_length,input_dim)
        embs = self.embeddings(inputs)
        embs_drop = self.drop(embs)
        #embs_drop = embs

        #print('Embs: ', embs.size())
        pack_input = pack_padded_sequence(embs_drop, input_lengths.data.cpu().numpy(), True)
        # rnn_out = tensor(batch_size, seq_length, rnn_dim * 2)
        # hc_n = (h_n,c_n); h_n = tensor(2, batch_size, rnn_dim)
        rnn_out, hc_n = self.hidden_layer(pack_input)
        rnn_out, _ = pad_packed_sequence(rnn_out, batch_first=True)
        #rnn_out = torch.tensor(rnn_out)

        if type(hc_n) == tuple:
            h_n = torch.cat([hc_n[0][0, :, :], hc_n[0][1, :, :]], -1)
        else:
            h_n = torch.cat([hc_n[0, :, :], hc_n[1, :, :]], -1)
        
        if self.attention:
            #a_hidden = F.relu(self.att_hidden(rnn_out))
            a_hidden = rnn_out
            # a_alpha = tensor(batch_size, seq_length, 1)
            a_alpha = self.att_alpha(torch.tanh(a_hidden))
            # a_alpha = tensor(batch_size, seq_length)
            a_alpha.squeeze_()
            
            # alpha = tensor(batch_size, seq_length)
            alpha = self.att_norm(a_alpha)
            
            # att_out = tensor(batch_size, seq_length, input_dim)
            att_out = rnn_out*alpha.view(batch_size,seq_length,1)
            #att_out = embs_drop*alpha.view(batch_size,seq_length,1)
            # att_h = tensor(batch_size, input_dim)
            att_h = att_out.sum(1)

            att_h = torch.cat([h_n, att_h], -1)
            #att_hidden = torch.cat([att_h, att_h], -1)
            att_hidd = h_n.view(batch_size,1,-1)*alpha.view(batch_size,-1,1)
            all_att = torch.cat([rnn_out,att_hidd], -1)

            return att_h, all_att, rnn_out
            
        else:
            # concatenate forward and backward h_n; h_n = tensor(batch_size, rnn_dim*2)
            return h_n,_, rnn_out
        

    def random_embedding(self, size, dim):
        pre_embs = np.empty([size, dim])
        scale = np.sqrt(3.0 / dim)
        for index in range(size):
            pre_embs[index,:] = np.random.uniform(-scale, scale, [1, dim])
        return pre_embs
        
    def set_zeros(self,idx):
        for i in idx:
            self.embeddings.weight.data[i].fill_(0)


class BiRNN(nn.Module):
    """
    This module take embedding inputs (characters or words) feeding to an RNN layer before adding a softmax function for classification
    """
    def __init__(self, word_HPs=None, num_labels=None):
        super(BiRNN, self).__init__()
        [nnmode, word_size, word_dim, wd_embeddings, word_hidden_dim,
         word_dropout, word_layers, word_bidirect, zero_padding, word_att, use_cuda] = word_HPs
         #[word_size, word_dim, word_pre_embs, word_hidden_dim, word_dropout, word_layers, word_bidirect] = word_HPs
        #[nnmode, size, dim, pre_embs, hidden_dim, dropout, layers, bidirect, zero_padding, attention] = HPs
        device = torch.device("cuda:0" if use_cuda else "cpu")
        word_dim_emb = word_dim
        word_HPs = [nnmode, word_size, word_dim_emb , wd_embeddings, word_hidden_dim, word_dropout, word_layers, word_bidirect, zero_padding, word_att, use_cuda]
        self.use_cuda = use_cuda
        self.bidirect = True
        self.layers = word_layers
        self.num_labels = num_labels
                

        self.hidden_dim = word_hidden_dim
        self.drop = nn.Dropout(0.3)
        self.rnn = Embs(word_HPs)
        
        
        
        if word_bidirect:
            if self.bidirect:
                #GNU
                self.lstm = nn.GRU(word_hidden_dim*2, self.hidden_dim // 2, num_layers=word_layers, batch_first=True, bidirectional=self.bidirect)
                #LSTM
                #self.lstm = nn.LSTM(word_hidden_dim*2, self.hidden_dim, num_layers=word_layers, batch_first=True, bidirectional=self.bidirect)
            else:
                self.lstm = nn.GRU(word_hidden_dim*2, self.hidden_dim // 2, num_layers=word_layers, batch_first=True, bidirectional=self.bidirect)
                #self.lstm = nn.LSTM(self.hidden_dim*2, self.hidden_dim, num_layers=word_layers, batch_first=True, bidirectional=self.bidirect)    
        else:
            if self.bidirect:
                self.hidden_dim = word_hidden_dim // 2
                self.lstm = nn.LSTM(self.hidden_dim, self.hidden_dim, num_layers=word_layers, batch_first=True, bidirectional=self.bidirect) 
            else:
                self.lstm = nn.LSTM(self.hidden_dim // 2, self.hidden_dim, num_layers=word_layers, batch_first=True, bidirectional=self.bidirect)    

        # ----CNN-------
        CNN_HPs = [self.hidden_dim,self.hidden_dim]
        self.convn = ConvNet(CNN_HPs,num_labels)
        #-----------------------------------------
        self.attention = word_att
        self.dropfinal = nn.Dropout(0.1)
        self.tanh = nn.Tanh()
        if self.attention:
            self.att_hidden = nn.Linear(self.hidden_dim*2, self.hidden_dim*2)
            self.att_alpha = nn.Linear(self.hidden_dim*2,1, bias=True)
            self.att_norm = nn.Softmax(-1)
        if num_labels > 2:
            self.fc1 = nn.Linear(self.hidden_dim*3, self.hidden_dim*2)
            self.fc2 = nn.Linear(self.hidden_dim*2, self.hidden_dim)
            
            self.hidden2tag = nn.Linear(self.hidden_dim, num_labels)
            self.lossF = nn.CrossEntropyLoss()
            #self.lossF = F.cross_entropy(lt, ls)
        else:
            self.hidden2tag = nn.Linear(hidden_dim, 1)
            self.lossF = nn.BCEWithLogitsLoss()
        if use_cuda:
            
            self.rnn = self.rnn.cuda(torch.device('cuda:0'))
            self.lossF = self.lossF.cuda(torch.device('cuda:2'))
            self.convn = self.convn.cuda(torch.device('cuda:2'))
            self.lstm = self.lstm.cuda(torch.device('cuda:1'))
            self.att_hidden = nn.DataParallel(self.att_hidden)
            self.att_alpha = nn.DataParallel(self.att_alpha)
            
   
    def forward2(self, word_tensor, word_lengths):
        """
            BiLSTM-BiGNU
        """
        #word_h_n, bi_rnn_out = self.rnn(word_tensor, word_lengths)
        _,att_h2 = self.get_hiddens(word_tensor, word_lengths)
        #att_h2 = att_h2.cpu()
        att_h2 = self.dropfinal(att_h2)
        att_h2 = self.fc1(F.relu(att_h2))
        att_h2 = self.dropfinal(att_h2)
        att_h2 = self.fc2(F.relu(att_h2))
        label_score = self.hidden2tag(att_h2)
        #print(word_h_n.shape, label_score.shape)
        #label_score = self.hidden2tag(att_hidd)
        #label_score = self.dropfinal(label_score)

        return label_score,_
    def forward(self, word_tensor, word_lengths):
        """
            BiLSTM-CNN
        """
        _,_,word_embs = self.rnn.get_last_hiddens(word_tensor, word_lengths)
        
        #hidd_lstm,_,word_embs = self.rnn.get_last_hiddens(word_tensor, word_lengths)
        label_score = self.convn(word_embs)

        return label_score,_

    

    def get_hiddens(self, word_inputs, word_lengths):
        """
        Out put : last_hidden, last_atthidden, all_hidden
        """
        word_batch = word_inputs.size(0)
        seq_length = word_inputs.size(1)

        att_h1,word_embs,_ = self.rnn.get_last_hiddens(word_inputs, word_lengths)
        print(word_embs.size())
         
        #embs_drop = self.drop(word_embs)
        embs_drop = self.dropfinal(word_embs)
        hc_0 = self.initHidden(word_batch)
        h_0 = self.initHiddenGNU(word_batch)
        #hc_0 = [[Variable(torch.zeros(self.layers*2, word_batch, self.hidden_dim))],[Variable(torch.zeros(self.layers*2, word_batch, self.hidden_dim))]]
        pack_input = pack_padded_sequence(embs_drop, word_lengths.data.cpu().numpy(), True)
        #pack_input_cu = pack_input
        #pack_input_cu = pack_input_cu.cuda()
        #rnn_out, hc_n = self.lstm(pack_input, hc_0)
        rnn_out, hc_n = self.lstm(pack_input,h_0)
        #rnn_out, hc_n = self.hidden_layer_bi(pack_input)
        rnn_out, _ = pad_packed_sequence(pack_input, batch_first=True)
        print('Hidden: ',hc_n.size())
        #h_n = torch.cat([hc_n[0][0,:,:], hc_n[0][1,:,:]],-1)
        #h_n = torch.cat([hc_n[0, :, :], hc_n[1, :, :]], -1)
        if type(hc_n) == tuple:
            h_n = torch.cat([hc_n[0][0, :, :], hc_n[0][1, :, :]], -1)
        else:
            h_n = torch.cat([hc_n[0, :, :], hc_n[1, :, :]], -1)
        #print(rnn_out.size())
        if self.attention:
            a_hidden = F.relu(self.att_hidden(rnn_out))
            #a_hidden = rnn_out
            # a_alpha = tensor(batch_size, seq_length, 1)
            a_alpha = self.att_alpha(torch.tanh(a_hidden))
            # a_alpha = tensor(batch_size, seq_length)
            a_alpha.squeeze_()
            
            # alpha = tensor(batch_size, seq_length)
            alpha = self.att_norm(a_alpha)
            
            # att_out = tensor(batch_size, seq_length, input_dim)
            att_out = rnn_out*alpha.view(word_batch,seq_length,1)
            #att_out = embs_drop*alpha.view(batch_size,seq_length,1)
            # att_h = tensor(batch_size, input_dim)
            att_h = att_out.sum(1)
            print('Att: ',att_h.size())
            att_h = torch.cat([h_n, att_h], -1)
            #print(att_h.size())
        return rnn_out,att_h
    
    def NLL_loss(self, label_score, label_tensor):
        #label_tensor = torch.autograd.Variable(label_tensor)
        if self.num_labels > 2:
            batch_loss = self.lossF(label_score, label_tensor)
            #batch_loss = F.cross_entropy(label_score, label_tensor)
        else:
            batch_loss = self.lossF(label_score, label_tensor.float().view(-1,1))
        return batch_loss  

    def inference(self, label_score, k=1):
        if self.num_labels > 2:
            label_prob = F.softmax(label_score, dim=-1)
            label_prob, label_pred = label_prob.data.topk(k)
        else:
            label_prob = torch.sigmoid(label_score.squeeze())
            label_pred = (label_prob >= 0.5).data.long()
        return label_prob, label_pred
#-----hoang code--------------

    def initHidden(self, batch_size):
        d = 2 if self.bidirect else 1
        h = Variable(torch.zeros(self.layers*d, batch_size, self.hidden_dim))
        c = Variable(torch.zeros(self.layers*d, batch_size, self.hidden_dim))
        if self.use_cuda:
            return h.cuda(), c.cuda()
        else:
            return h, c

    def initHiddenGNU(self, batch_size):
        d = 2 if self.bidirect else 1
        h = Variable(torch.zeros(self.layers*d, batch_size, self.hidden_dim // 2))
        #c = Variable(torch.zeros(self.layers*d, batch_size, self.hidden_dim))
        if self.use_cuda:
            return h.cuda()#, c.cuda()
        else:
            return h#, c

#---------end----------------

class ConvNet(nn.Module):
    def __init__(self, CNN_HPs=None, num_labels=None):
        super(ConvNet, self).__init__()
        """
        
        D = args.embed_dim
        C = args.class_num
        Ci = 1
        Co = args.kernel_num
        Ks = args.kernel_sizes
        """
        [D,Co]= CNN_HPs
        
        C = num_labels
        Ks = [1, 2, 3, 4, 5]
        Ci = 1
        N_F = 64
        self.convs1 = [nn.Conv2d(Ci, D, (K, D), stride=1, padding=(K//2, 0), bias=True) for K in Ks]
        self.convs2 = [nn.Conv2d(Ci, Co, (K, D), stride=1, padding=(K//2, 0), bias=True) for K in Ks]
        #with torch.cuda.device(1):
        for conv in self.convs1:
            conv = conv.cuda()
            #conv = nn.DataParallel(conv)
        # for cnn cuda
        for conv in self.convs2:
            conv = conv.cuda()
            #conv = nn.DataParallel(conv)

       
        in_fea = len(Ks) * Co
        self.drop1 = nn.Dropout(0.3)
        self.fc1 = nn.Linear(in_features=in_fea, out_features= in_fea // 2, bias=True)
        self.drop2 = nn.Dropout(0.2)
        self.fc3 = nn.Linear(in_features=in_fea // 2, out_features= D, bias=True)
        self.fc2 = nn.Linear(in_features=D, out_features=C, bias=True)

        self.fc1 = self.fc1.cuda()
        
        self.fc2 = self.fc2.cuda()
        self.fc3 = self.fc3.cuda()

        
        

    def forward(self, x):
        #x.size()  # (N,W,D) #  torch.Size([batch_size, num_word, hidden_dim)
        shap = x.size()
        
        #x_norm = nn.BatchNorm1d(shap[1])
        #x_norm = x_norm.cuda()
        #x = x_norm(x)
        one_layer = x.unsqueeze(1)  # (N,Ci,W,D)  #  torch.Size([64, 1, 43, 300])
        
       
        # one layer
        #print(one_layer.size())
        one_layer = [torch.transpose(F.relu(conv(F.relu(one_layer))).squeeze(3), 1, 2) for conv in self.convs1] # torch.Size([64, 100, 36])
        #with torch.cuda.device(1):
           
    
        # two layer
        two_layer = [F.relu(conv(one_layer.unsqueeze(1))).squeeze(3) for (conv, one_layer) in zip(self.convs2, one_layer)]
        
        # pooling
        output = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in one_layer]
        output = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in two_layer]   #  torch.Size([64, 100]) torch.Size([64, 100])
        output = torch.cat(output, 1)  # torch.Size([64, 300])
        print('Output pool',output.size())
        # dropout
        #output = self.dropout(output)
        # linear
        
        output = self.drop1(output)
        output = self.fc1(F.relu(output))
        output = self.drop2(output)
        output = self.fc3(F.relu(output))
        logit = self.fc2(F.relu(output))
        #print(logit.shape)
        label_score1 = torch.zeros(shap[0], 5)
        label_score1 = logit
        return label_score1
  
if __name__ == "__main__":
    from data_utils import Data2tensor, Vocab, seqPAD, Txtfile
    filename = "../data/train.txt"
    vocab = Vocab(wl_th=None, cutoff=2)
    vocab.build([filename], firstline=False)
    word2idx = vocab.wd2idx(vocab.w2i)
    tag2idx = vocab.tag2idx(vocab.l2i)
    train_data = Txtfile(filename, firstline=False, word2idx=word2idx, tag2idx=tag2idx)

    train_iters = Vocab.minibatches(train_data, batch_size=4)
    data = []
    label_ids = []
    for words, labels in train_iters:
        data.append(words)
        label_ids.append(labels)
        word_ids, sequence_lengths = seqPAD.pad_sequences(words, pad_tok=0, wthres=1024)

    w_tensor = Data2tensor.idx2tensor(word_ids)
    y_tensor = Data2tensor.idx2tensor(labels)

    data_tensors = Data2tensor.sort_tensors(labels, word_ids, sequence_lengths)
    label_tensor, word_tensor, sequence_lengths, word_seq_recover = data_tensors
