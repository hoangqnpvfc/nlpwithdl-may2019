import os
os.chdir('I://NLPProject/nlpwithdl-may2019')
import numpy as np
import pandas as pd
from sklearn import model_selection, preprocessing
from sklearn.feature_extraction.text import TfidfVectorizer
import my_networkw2 as nn

def get_label(_lb):
    
    tam = True
    tlabel = []
    for index, cate in enumerate(_lb):
        if (index == 0):
            tlabel.append(cate)
        else:
            for i, c in enumerate(tlabel):
                if( cate != c):
                    tam = True
                else:
                    tam = False
                    break
            if (tam == True):
                tlabel.append(cate)
    return tlabel

os.chdir('I://NLPProject/nlpwithdl-may2019')
df = pd.read_csv('my_datasetw2.csv')
#random.shuffle(df)
train_x, test_x, train_y, test_y = model_selection.train_test_split(df['text'], df['label'])
train_y_fir = train_y
encoder = preprocessing.LabelEncoder()
train_y = encoder.fit_transform(train_y)
test_y = encoder.fit_transform(test_y)

tfidf_vect = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', max_features=5000)
tfidf_vect.fit(df['text'])
xtest_tfidf =  tfidf_vect.transform(test_x)
xtrain_tfidf =  tfidf_vect.transform(train_x)

tlabelnn = get_label(train_y)
tam_ynn = train_y

train_y = np.expand_dims(train_y, axis=0)
test_y = np.expand_dims(test_y, axis=0)

xtrain_tfidf =  xtrain_tfidf.T.toarray()
xtest_tfidf =  xtest_tfidf.T.toarray()


#Chinh sua tap Y t?o ra tap Y moi
train_ynn = np.zeros(shape=(len(tlabelnn), xtrain_tfidf.shape[1]))

for idx, cate in enumerate(tlabelnn):
    tam_tr = []
    for k, ct in enumerate(tam_ynn):
        if( ct == cate):
            tam_tr.append('1')
        else :
            tam_tr.append('0')
    train_ynn[idx,:] = tam_tr

train_ynn = np.expand_dims(train_ynn, axis=0)
#train_ynn1 = train_ynn.reshape(4, 2000)
# tao mang Neuron NetWork

net = nn.Network([5000, 7, 4])

L = net.num_layers
layer_dim = net.sizes

# Khoi tao W va b
params = net.parameters_deep(layer_dim)

for i in range(1, L):
	net.weights.append(params['W' + str(i)])
	net.biases.append(params['b' + str(i)])

params = net.Train_L_layer_model(xtrain_tfidf, train_ynn, learning_rate = 0.3, num_iterations = 5000, print_cost=True)
p = net.predict(xtrain_tfidf, train_ynn, params)
#F_AL, F_caches = net.L_model_forward(xtrain_tfidf, params)
"""
# Forward
F_AL, F_caches = net.L_model_forward(xtrain_tfidf, params)

#BackForward
grads = {}
grads = net.L_model_backward(F_AL, train_ynn, F_caches)

# Update W, b

#params = net.update_parameters(params, grads, 0.01)
"""
