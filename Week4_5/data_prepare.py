from __future__ import unicode_literals, print_function, division
from io import open
import unicodedata
import string
import re
import random

import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F

from collections import Counter

SOS_token = 0
EOS_token = 1

DATA_PATH = "./dataset/"
MAX_LENGTH = 10

eng_prefixes = (
    "i am ", "i m ",
    "he is", "he s ",
    "she is", "she s ",
    "you are", "you re ",
    "we are", "we re ",
    "they are", "they re "
)


class Lang:
    def __init__(self, name):
        self.name = name
        self.word2index = {}
        self.word2count = {}
        self.index2word = {0: "SOS", 1: "EOS"}
        self.n_words = 2  # Count SOS and EOS

    def addSentence(self, sentence):
        for word in sentence.split(' '):
            self.addWord(word)

    def addWord(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.word2count[word] = 1
            self.index2word[self.n_words] = word
            self.n_words += 1
        else:
            self.word2count[word] += 1


class ProcessData:

    def __init__(self, data_name, path, lang1, lang2, sentences_filter = None,
                 data_type = ".txt", max_length = 10):
        self.name = data_name
        self.data_path = path
        self.encoding = lang1
        self.decoding = lang2
        self.data_type = data_type
        self.max_length = max_length
        self.filter = sentences_filter


    # Turn a Unicode string to plain ASCII, thanks to
    # https://stackoverflow.com/a/518232/2809427
    def unicodeToAscii(self, s):
        return ''.join(
            c for c in unicodedata.normalize('NFD', s)
            if unicodedata.category(c) != 'Mn'
        )


    # Lowercase, trim, and remove non-letter characters
    def normalizeString(self, s):
        s = self.unicodeToAscii(s.lower().strip())
        s = re.sub(r"([.!?])", r" \1", s)
        s = re.sub(r"[^a-zA-Z.!?]+", r" ", s)
        return s

    def readLangs(self, reverse=False):
        print("Reading lines...")
        data_path = self.data_path + self.encoding + '_' + self.decoding + '/' + self.name + self.data_type
        # Read the file and split into lines
        lines = open(data_path, encoding='utf-8').\
            read().strip().split('\n')

        # Split every line into pairs and normalize
        pairs = [[self.normalizeString(s) for s in l.split('\t')] for l in lines]

        # Reverse pairs, make Lang instances
        if reverse:
            pairs = [list(reversed(p)) for p in pairs]
            input_lang = Lang(self.decoding)
            output_lang = Lang(self.encoding)
        else:
            input_lang = Lang(self.encoding)
            output_lang = Lang(self.decoding)

        return input_lang, output_lang, pairs


    def filterPair(self, p):
        return len(p[0].split(' ')) < self.max_length and \
            len(p[1].split(' ')) < self.max_length and \
            p[1].startswith(self.filter)


    def filterPairs(self, pairs):
        return [pair for pair in pairs if self.filterPair(pair)]


    def prepareData(self, reverse=False):
        input_lang, output_lang, pairs = self.readLangs(reverse)
        print("Read %s sentence pairs" % len(pairs))
        pairs = self.filterPairs(pairs)
        print("Trimmed to %s sentence pairs" % len(pairs))
        print("Counting words...")
        for pair in pairs:
            input_lang.addSentence(pair[0])
            output_lang.addSentence(pair[1])
        print("Counted words:")
        print(input_lang.name, input_lang.n_words)
        print(output_lang.name, output_lang.n_words)
        return input_lang, output_lang, pairs

class SentenceTensor:

    def __init__(self, lang1, lang2, pairs, n_iters, device):
        self.input_lang = lang1
        self.output_lang = lang2
        self.pairs = pairs
        self.n_iters = n_iters
        self.device = device

    def indexesFromSentence(self, lang, sentence):
        return [lang.word2index[word] for word in sentence.split(' ')]


    def tensorFromSentence(self, lang, sentence):
        indexes = self.indexesFromSentence(lang, sentence)
        indexes.append(EOS_token)
        return torch.tensor(indexes, dtype=torch.long, device=self.device).view(-1, 1)


    def tensorsFromPair(self, pair):
        input_tensor = self.tensorFromSentence(self.input_lang, pair[0])
        target_tensor = self.tensorFromSentence(self.output_lang, pair[1])
        return (input_tensor, target_tensor)

    def trainingTensor(self):
        return [self.tensorsFromPair(random.choice(self.pairs))
                      for i in range(self.n_iters)]


# package tensor_lang[input_lang, ouput_lang, pairs, device]
class Vocab:
    def __init__(self, tensor_lang, SOS_token, EOS_token,
    max_length = 10):
        self.tensor_lang = tensor_lang
        self.SOS_token = SOS_token
        self.EOS_token = EOS_token
        self.max_length = max_length

 