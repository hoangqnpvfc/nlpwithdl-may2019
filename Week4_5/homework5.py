import random
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.ticker as ticker
import data_prepare
import model
SOS_token = 0
EOS_token = 1
hidden_size = 256
DATA_PATH = "./dataset/"
MAX_LENGTH = 10
n_iters = 75000
print_every=5000

eng_prefixes = (
    "i am ", "i m ",
    "he is", "he s ",
    "she is", "she s ",
    "you are", "you re ",
    "we are", "we re ",
    "they are", "they re "
)
process_data = data_prepare.ProcessData(
    data_name = 'train',
    path = DATA_PATH,
    lang1 = 'en',
    lang2 = 'fr',
    sentences_filter = eng_prefixes)
input_lang, output_lang, pairs = process_data.prepareData(True)
print(random.choice(pairs))

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
tensor_lang = data_prepare.SentenceTensor(
    lang1 = input_lang,
    lang2 = output_lang,
    pairs = pairs,
    n_iters = 75000,
    device = device)

vocab = data_prepare.Vocab(
    tensor_lang = tensor_lang,
    SOS_token = SOS_token,
    EOS_token = EOS_token)

encoder1 = model.EncoderRNN(input_lang.n_words, hidden_size).to(device)
attn_decoder1 = model.AttnDecoderRNN(hidden_size, output_lang.n_words, dropout_p=0.1).to(device)

seq2seq = model.Seq2Seq(
    vocab = vocab,
    encoder = encoder1,
    decoder = attn_decoder1,
    n_iters = n_iters,
    device = device)


seq2seq.trainIters()
seq2seq.evaluateRandomly()

output_words, attentions = seq2seq.evaluate("je suis trop froid .")
plt.matshow(attentions.numpy())