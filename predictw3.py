import torch
from utils.data_utils import SaveloadHP, Txtfile, Data2tensor, seqPAD, Vocab
from model import Languagemodel
from utils.core_nns import RNNModel
import numpy as np

PAD = u"<PAD>"

def load_model():
    args=SaveloadHP.load("./results/lm.args")
    args.use_cuda = False
    lmd=Languagemodel(args)
    lmd.model.load_state_dict(torch.load('./results/lm.m'))
    print(lmd.model)
    return lmd

def rev_gen(languageModel):
    """
    train_file = "I://Full VLTKI/NLP_May2019/Week#3/nnlm/dataset/train.small.txt"
    dev_file = "I://Full VLTKI/NLP_May2019/Week#3/nnlm/dataset/val.small.txt"
    test_file = 'I://Full VLTKI/NLP_May2019/Week#3/nnlm/dataset/test.small.txt'

    test_data = Txtfile(test_file, firstline=False, source2idx=languageModel.word2idx, label2idx=languageModel.label2idx)  
    data_files =["I://Full VLTKI/NLP_May2019/Week#3/nnlm/dataset/test.small.txt"]
    """

    args = languageModel.args
    ntokens = len(args.vocab.w2i)
    word2idx = args.vocab.wd2idx(args.vocab.w2i,allow_unk=args.allow_unk, start_end=args.se_words)
    label2idx = args.vocab.tag2idx(args.vocab.l2i)
    word2idx = args.vocab.wd2idx(args.vocab.w2i,allow_unk=args.allow_unk, start_end=args.se_words)
    test_data = Txtfile(args.test_file, firstline=False, source2idx=word2idx, label2idx=label2idx)
    test_batch = args.vocab.minibatches(test_data, batch_size=args.batch_size)

    input_aa = torch.randint(ntokens, (1, 1), dtype=torch.long)
    words_gen = 1000
    hidden = languageModel.model.init_hidden(1)
    outf='./results/generated.txt'
    with open(outf, 'w') as outf:
        with torch.no_grad():    
            for i in range(words_gen):                                             
                output, hidden = languageModel.model(input_aa, hidden)
                word_weights = output.squeeze().div(1.0).exp().cpu()
                word_idx = torch.multinomial(word_weights, 1)[0]
                input_aa.fill_(word_idx)            
                word=args.vocab.i2w[word_idx.tolist()]
                outf.write(word + ('\n' if i % 20 == 19 else ' '))    

def infer(self, label_score, k=1):
        label_prob = F.softmax(label_score, dim=-1)
        label_prob, label_pred = label_prob.data.topk(k)
        return label_pred
def predict(languageModel):
    
    # Perform forward propagation and return index of the highest score
    args = languageModel.args
    vocab = args.vocab
    #vocab.build(data_files, firstline=False)
    word2idx = vocab.wd2idx(vocab.w2i)
    label2idx = vocab.tag2idx(vocab.l2i)
    
     
    train_data = Txtfile(args.train_file, firstline=False, source2idx=word2idx, label2idx=label2idx)
    # train_data = [sent[0] for sent in train_data]
    train_batch = args.vocab.minibatches(train_data, batch_size=args.batch_size)
    #train_batch = languageModel.train_batch(train_data)
    
    inpdata=[]
    outdata=[]
    for sent in train_batch:
        word_pad_ids, seq_lens = seqPAD.pad_sequences(sent, pad_tok=vocab.w2i[PAD])
        data_tensor = Data2tensor.idx2tensor(word_pad_ids)
        for i in range(0, data_tensor.size(1)-1, args.bptt):
            data, target = vocab.bptt_batch(data_tensor, i, args.bptt)
            inpdata.append(data)
            outdata.append(target)
        break

    #print(inpdata[9])
    hidden = languageModel.model.init_hidden(args.batch_size)
    output, hidden = languageModel.model(inpdata[0], hidden)
    print(output)
    deco, hidden = languageModel.model.forward(inpdata[0], hidden)
    _ , pred = languageModel.model.inference(deco)
    
    print(pred)
    
if __name__ == '__main__':
    model=load_model()
    

    rev_gen(model)

    #test_file = 'I://Full VLTKI/NLP_May2019/Week#3/nnlm/dataset/test.small.txt'

    #test_data = Txtfile(x, firstline=False, source2idx=languageModel.word2idx, label2idx=languageModel.label2idx)
    predict(model)
